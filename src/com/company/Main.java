package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int number;
        Scanner input = new Scanner(System.in);
        System.out.println("請輸入一個數,判斷是否為奇數:");
        number = input.nextInt();
        if ((number & 1) == 0)
            System.out.printf("%d不為奇數", number);
        else System.out.printf("%d為奇數", number);

    }
}
